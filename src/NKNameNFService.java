import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/name")
public class NKNameNFService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getNameNFService")
	public ArrayList<String> getNameNFService(){
		System.out.println("NKNameNFService.getNameNFService() called ..... ");
		ArrayList<String> names = new NKDAO().getNameNFDAO();
		return names;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postNameNFService")
	public ArrayList<String> postNameNFService(){
		System.out.println("NKNameNFService.postNameNFService() called ..... ");
		ArrayList<String> names = new NKDAO().getNameNFDAO();
		return names;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/putNameNFService")
	public Boolean putNameNFService(NKNameBean nkNameBean){
		System.out.println("NKNameService.putNameNFService() called: ..... "+nkNameBean);
		Boolean result = new NKDAO().putNameNFDAO(nkNameBean);
		return result;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postNameNFService")
	public Boolean postNameNFService(NKNameBean nkNameBean){
		System.out.println("NKNameService.postNameNFService() called: ..... "+nkNameBean);
		Boolean result = new NKDAO().postNameNFDAO(nkNameBean);
		System.out.println("postNameNFService() result is... "+result);
		return result;
	}

}
