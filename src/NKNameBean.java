
public class NKNameBean {
	
	// Defining Attributes for the Bean
	String name;
	String meaning;
	// String theme;
	// String language;
	// String rashi;
	// String nakshatra;
	String gender;
	// String synonyms;
		
	NKNameBean(){
		name 	= "Aditya"	;
		meaning = "Sun"		;
		gender 	= "Male"	;	
	}
	
	// Defining GETTER & SETTER methods
	String getName(){
		return this.name;
	}
	void setName(String name){
		this.name = name;
	}
	String getMeaning(){
		return this.meaning;
	}
	void setMeaning(String meaning){
		this.meaning = meaning;
	}
	String getGender(){
		return this.gender;
	}
	void setGender(String gender){
		this.gender = gender;
	}
	// Overriding toString() method
	public String toString(){
		return "Name: "+this.name+" Meaning: "+this.meaning+" Gender: "+this.gender;
	}
}
