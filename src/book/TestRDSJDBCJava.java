package book;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/* Java Code to interact with the MySQL database */
class TestRDSJDBCJAva{
	public static void main(String args[]){
		// Code to connect with MySQL via JDBC.
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(  
			"jdbc:mysql://nkdata.c0ukmdgun4ne.us-west-2.rds.amazonaws.com:3306","nkadmin","nkpassword");  
			//here sonoo is database name, root is username and password  
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from name");  
			while(rs.next())  
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));  
			con.close();  
		}
		catch(Exception e){
			System.out.println("Exception raised: "+e);
		}
	}
}
