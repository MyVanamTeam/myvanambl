package book;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

//import com.mysql.jdbc.Connection;

// Annotations
@Path("/book")  // Defining URI/URL

public class Book {
	
	/* 
	@GET		// HTTP
	@Produces(MediaType.APPLICATION_XML)		// @Consumes used with POST annotation
	public String sayHelloXML(){
		String response = "<?xml version='1.0'?><hello>Hello XML</hello>";
		return response;
	}
	
	@GET		// HTTP
	@Produces(MediaType.TEXT_HTML)		// @Consumes used with POST annotation
	public String sayHelloHTML(){
		String response = "<h1>Hello World!</h1><p>This is my First RESTful webservice using Jersey</p>";
		return response;
	}
	*/
	
	@GET 
	@Path("/getName")
	@Produces(MediaType.APPLICATION_XML)
	public String getName(){
		NameBean nb = new NameBean();
		String response = "<?xml version='1.0'?>"+
							"<names>"+
							"<name>"+nb.getName()+"</name>"+
							"<meaning>"+nb.getMeaning()+"</meaning>"+
							"<gender>"+nb.getGender()+"</gender>"+
							"</names>";
		System.out.println("Connecting MySQL database");
		new NamakaranamJDBC().connectMySQL();
		return response;
	}
	
	/* 
	@GET
	@Produces(MediaType.APPLICATION_JSON)		// @Consumes used with POST annotation
	public String sayHelloJSON(){
		String response = null;
		return response;
	}
	*/
}

class NameBean{
	
	// Defining Attributes
	String name;
	String meaning;
	// String theme;
	// String language;
	// String rashi;
	// String nakshatra;
	String gender;
	// String synonyms;
	
	// Defining GETTER & SETTER methods
	String getName(){
		return this.name;
	}
	void setName(String name){
		this.name = name;
	}
	String getMeaning(){
		return this.meaning;
	}
	void setMeaning(String meaning){
		this.meaning = meaning;
	}
	String getGender(){
		return this.gender;
	}
	void setGender(String gender){
		this.gender = gender;
	}
	
	NameBean(){
		name = "Aditya";
		meaning = "Sun";
		gender = "Male";	
	}
}

/* Java Code to interact with the MySQL database */
class NamakaranamJDBC{
	void connectMySQL(){
		// Code to connect with MySQL via JDBC.
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(  
			"jdbc:mysql://localhost:3306/namakaranam","root","matrusri");  
			//here sonoo is database name, root is username and password  
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from name");  
			while(rs.next())  
			System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));  
			con.close();  
		}
		catch(Exception e){
			System.out.println("Exception raised: "+e);
		}
	}
}