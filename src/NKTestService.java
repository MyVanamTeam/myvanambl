import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/name")
public class NKTestService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getTestService")
	public List<NKTestBean> getTestService(){
		System.out.println("NKTestService.getTestService() called: ");
		ArrayList<NKTestBean> alNKTestBean = new NKDAO().getTestDAO();
		return alNKTestBean;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postTestService")
	public List<NKTestBean> postTestService(){
		System.out.println("NKTestService.postTestService() called: ");
		ArrayList<NKTestBean> alNKTestBean = new NKDAO().getTestDAO();
		return alNKTestBean;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/putTestService")
	public Boolean putTestService(NKTestBean nkTestBean){
		System.out.println("NKTestService.putTestService() called: ......"+nkTestBean);
		System.out.println(nkTestBean.getSubmitter());
		/* NKTestBean nkTestBean = new NKTestBean();
		nkTestBean.setSubmitter("Mani Kiran");
		nkTestBean.setMessage("Kiran's Message");
		nkTestBean.setEmail("kiran@namakaranam.com");*/
		Boolean result = new NKDAO().putTestDAO(nkTestBean);
		return result;
		// return "{name:Hello World!}";
		// return nkTestBean;
	}
}