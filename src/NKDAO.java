import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class NKDAO {
	
	/* String dbUrl 	= "jdbc:mysql://localhost:3306/namakaranam";
	String dbUsername 	= "root";
	String dbPassword 	= "matrusri"; */
	String dbUrl 		= "jdbc:mysql://namakaranam.cra0jtetvyzx.us-east-2.rds.amazonaws.com:3306/NAMAKARANAM";
	String dbUsername 	= "nkadmin";
	String dbPassword 	= "nkpassword";
	
	public NKNameBean getNameDAO(String name){
		NKNameBean nkNameBean = null;
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */
			// here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = "SELECT name, meaning, gender FROM NAMES WHERE name like '"+name+"%'";
			System.out.println("NKDAO.getNameDAO(): Query submitted is: "+query);
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next()){  
				System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
				nkNameBean = new NKNameBean();
				nkNameBean.setName(rs.getString(1));
				nkNameBean.setMeaning(rs.getString(2));
				nkNameBean.setGender(rs.getString(3));
			}
			con.close();  
			return nkNameBean;			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.getNameDAO: "+e);
		}	
		return null;	
	}
	
	
	public ArrayList<NKNameBean> getNameDAO(String gender,String startsWith,String contains,String endsWith, String theme, String language){
		NKNameBean nkNameBean = null;
		ArrayList<NKNameBean> alNKNameBean = new ArrayList<NKNameBean>();
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */
			/* "jdbc:mysql://nkdata.c0ukmdgun4ne.us-west-2.rds.amazonaws.com:3306/Namakaranam","nkadmin","nkpassword"); */
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = 	"SELECT name, meaning, gender FROM NAMES WHERE 1=1";
			System.out.println("-"+startsWith+"-"+endsWith+"-"+contains+"-"+gender+"-"+theme+"-"+language+"-");
			if( (gender != null) 		&& (!gender.equals("")) 	){ query += " AND gender='"+gender+"' ";	 	 }
			if( (startsWith != null) 	&& (!startsWith.equals(""))	){ query += " AND name like '"+startsWith+"%' "; }
			if( (endsWith != null) 		&& (!endsWith.equals("")) 	){ query += " AND name like '%"+endsWith+"' ";	 }
			if( (contains != null) 		&& (!contains.equals("")) 	){ query += " AND name like '%"+contains+"%' ";	 }
			if( (theme != null)         && (!theme.equals(""))      ){ query += " AND theme='"+theme+"' ";           }
			if( (language != null) 		&& (!language.equals("")) 	){ query += " AND language='"+language+"' ";	 }	
			
			System.out.println("NKDAO.getNameDAO(): Query submitted is: "+query);
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next()){  
				// System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
				nkNameBean = new NKNameBean();
				nkNameBean.setName(rs.getString(1));
				nkNameBean.setMeaning(rs.getString(2));
				nkNameBean.setGender(rs.getString(3));
				alNKNameBean.add(nkNameBean);
			}
			con.close();  
			return alNKNameBean;			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.getNameDAO: "+e);
		}	
		return null;	
	}
	
	
	public ArrayList<NKTestBean> getTestDAO(){
		NKTestBean nkTestBean = null;
		ArrayList<NKTestBean> alNKTestBean = new ArrayList<NKTestBean>();
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = 	"SELECT submitter, message, email FROM TESTIMONIALS";
			
			System.out.println("NKDAO.getTestDAO(): Query submitted is: "+query);
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next()){  
				System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
				nkTestBean = new NKTestBean();
				nkTestBean.setSubmitter(rs.getString(1));
				nkTestBean.setMessage(rs.getString(2));
				nkTestBean.setEmail(rs.getString(3));
				alNKTestBean.add(nkTestBean);
			}
			con.close();  
			return alNKTestBean;			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.getTestDAO: "+e);
		}	
		return null;	
	}
	
	
	public Boolean putTestDAO(NKTestBean nkTestBean){
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */ 
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = 	"INSERT INTO TESTIMONIALS(submitter, message, email) "+
							" VALUES('"+nkTestBean.getSubmitter()+"','"+nkTestBean.getMessage()+"','"+nkTestBean.getEmail()+"')";
			
			System.out.println("NKDAO.putTestDAO(): Query submitted is: "+query);
			int rs=stmt.executeUpdate(query);  
			con.close();  
			return (rs>0?true:false);			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.putTestDAO: "+e);
		}	
		return false;	
	}
	
	public Boolean putNameDAO(NKNameBean nkNameBean){
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */ 
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = 	"INSERT INTO NAMES(name,meaning,gender) "+
							" VALUES('"+nkNameBean.getName()+"','"+nkNameBean.getMeaning()+"','"+nkNameBean.getGender()+"')";
			
			System.out.println("NKDAO.putNameDAO(): Query submitted is: "+query);
			int rs=stmt.executeUpdate(query);  
			con.close();  
			return (rs>0?true:false);			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.putNameDAO: "+e);
		}	
		return false;	
	}
	
	public Boolean putNameNFDAO(NKNameBean nkNameBean){
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = 	"INSERT INTO NAMESNF(name) "+
							" VALUES('"+nkNameBean.name+"')";
			
			System.out.println("NKDAO.putNameNFDAO(): Query submitted is: "+query);
			int rs=stmt.executeUpdate(query);  
			con.close();  
			return (rs>0?true:false);			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.putNameNFDAO: "+e);
		}	
		return false;	
	}
	
	public Boolean postNameNFDAO(NKNameBean nkNameBean){
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */ 
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = 	"INSERT INTO NAMESNF(name) "+
							" VALUES('"+nkNameBean.name+"')";
			
			System.out.println("NKDAO.postNameNFDAO(): Query submitted is: "+query);
			int rs=stmt.executeUpdate(query);  
			con.close();  
			return (rs>0?true:false);			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.postNameNFDAO: "+e);
		}	
		return false;	
	}
	
	public ArrayList<String> getNameNFDAO(){
		NKNameBean nkNameBean = null;
		ArrayList<String> names = new ArrayList<String>();
		// Code to establish connection to MySQL via JDBC
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */ 
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			String query = "SELECT name FROM NAMESNF";
			System.out.println("NKDAO.getNameDAO(): Query submitted is: "+query);
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next()){  
				System.out.println(rs.getString(1));
				names.add(rs.getString(1));
			}
			con.close();  
			return names;
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.getNameDAO: "+e);
		}	
		return null;	
	}
	
	public ArrayList<NKNameBean> findNameDAO(NKFindNameBean nkFindNameBean){
		ArrayList<NKNameBean> alNKNameBean = new ArrayList<NKNameBean>();
		String name1 = nkFindNameBean.getName1();
		String name2 = nkFindNameBean.getName2();
		String gender = nkFindNameBean.getGender();
		// Code to establish connection to MySQL via JDBC
		try{
			
			// Getting the character sets for NAME1
			TreeSet<String> name1Set = new TreeSet<String>();
			for(int i = 1 ; i < name1.length() ; i++){
				for(int j = 0 ; j < name1.length()-i ; j++){
					String temp = name1.substring(j,j+1+i);
					name1Set.add(temp);
					// System.out.println("i: "+i+"  j:"+j+"  temp:"+temp+"  test: "+name1.substring(i));
				}
			}
			System.out.println("TreeSet for name1: "+name1Set);
			
			// Getting the character sets for NAME2
			TreeSet<String> name2Set = new TreeSet<String>();
			for(int i = 1 ; i < name2.length() ; i++){
				for(int j = 0 ; j < name2.length()-i ; j++){
					String temp = name2.substring(j,j+1+i);
					name2Set.add(temp);
					// System.out.println("i: "+i+"  j:"+j+"  temp:"+temp+"  test: "+name2.substring(i));
				}
			}
			System.out.println("TreeSet for name2: "+name2Set);
			
			// Forming the OR Query for NAME1
			Object name1SetArray[] = name1Set.toArray();
			String name1OR = "";
			for(int i = 0 ; i < name1SetArray.length ; i++){
				if(name1OR.equals("")){
					name1OR += " name LIKE '%"+name1SetArray[i]+"%' ";
				}
				else{
					name1OR += " OR name LIKE '%"+name1SetArray[i]+"%' ";
				}
			}
			System.out.println("name1 OR Query: "+name1OR);
			
			// Forming the OR Query for NAME2
			Object name2SetArray[] = name2Set.toArray();
			String name2OR = "";
			for(int i = 0 ; i < name2SetArray.length ; i++){
				if(name2OR.equals("")){
					name2OR += " name LIKE '%"+name2SetArray[i]+"%' ";
				}
				else{
					name2OR += " OR name LIKE '%"+name2SetArray[i]+"%' ";
				}
			}
			System.out.println("name2 OR Query: "+name2OR);
			
			// Forming the Final SQL Query
			String finalQuery = "SELECT name, meaning, gender FROM NAMES "+
								"WHERE ("+name1OR+") AND ("+name2OR+") ";
			
			if(!gender.equals("")){
				finalQuery += " AND ("+"gender='"+gender+"') ";
			}
								
			System.out.println("Printing the FINAL Query: "+finalQuery);
			
			// Running the QUERY against the Database
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
			/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */ 
			//here namakaranam is database name, root is username and password  
			Statement stmt=con.createStatement();
			ResultSet rs=stmt.executeQuery(finalQuery); 
			int counter = 0;
			while(rs.next()){
				counter++;
				// System.out.print(rs.getString(1));
				
				// Finding the match percentage for each RESULT NAME
				String resultName = rs.getString(1);
				int name1Counter = 0 ;
				for(int i = 0 ; i < name1SetArray.length ; i++){
					if(resultName.contains((String)name1SetArray[i])){
						name1Counter++;
					}
				}
				int name2Counter = 0 ;
				for(int i = 0 ; i < name2SetArray.length ; i++){
					if(resultName.contains((String)name2SetArray[i])){
						name2Counter++;
					}
				}
				double name1Percent = name1Counter * 100 / name1SetArray.length;
				double name2Percent = name2Counter * 100 / name2SetArray.length;
				double totalPercent = (name1Percent + name2Percent) / 2;
				
				if(totalPercent >= 30){
					NKNameBean nkNameBean = new NKNameBean();
					nkNameBean.setName(rs.getString(1));
					nkNameBean.setMeaning(rs.getString(2));
					nkNameBean.setGender(rs.getString(3));
					alNKNameBean.add(nkNameBean);
					System.out.println("resultName: "+resultName+"    name1Percent: "+name1Percent+"    name2Percent: "+name2Percent+"    totalPercent: "+totalPercent);
				}
				
			}
			System.out.println("\n# of names returned: "+counter);
			con.close();
			return alNKNameBean;			
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.getNameDAO: "+e);
		}	
		return null;	
	}
	
	public ArrayList<NKTwinNameBean> findTwinNameDAO(NKFindTwinNameBean nkFindTwinNameBean){
			// return alNKTwinNameBean;		
			System.out.println("In NKDAO.findTwinNameDAO(): "+nkFindTwinNameBean);
			NKTwinNameBean nkTwinNameBean = new NKTwinNameBean();
			ArrayList<NKTwinNameBean> alNKTwinNameBean = new ArrayList<NKTwinNameBean>();
			
			/* 
			nkTwinNameBean.setName1("MAHESH");
			nkTwinNameBean.setName2("SURESH");
			nkTwinNameBean.setMeaning1("Lord Shiva");
			nkTwinNameBean.setMeaning2("Lord Shiva");
			nkTwinNameBean.setGender1("B");
			nkTwinNameBean.setGender2("B");
			alNKTwinNameBean.add(nkTwinNameBean);*/
			
			// Code to establish connection to MySQL via JDBC
			try{
				Class.forName("com.mysql.jdbc.Driver");
				Connection con=DriverManager.getConnection(dbUrl,dbUsername,dbPassword);  
				/* "jdbc:mysql://localhost:3306/namakaranam","root","matrusri"); */
				/* "jdbc:mysql://nkdata.c0ukmdgun4ne.us-west-2.rds.amazonaws.com:3306/Namakaranam","nkadmin","nkpassword"); */
				//here namakaranam is database name, root is username and password  
				Statement stmt=con.createStatement();
				String query = 	"SELECT name1, name2, meaning1, meaning2, gender1, gender2 FROM TWINNAMES WHERE 1=1";
				/* if( (gender != null) 		&& (!gender.equals("")) 	){ query += " AND gender='"+gender+"' ";	 	 }
				if( (startsWith != null) 	&& (!startsWith.equals(""))	){ query += " AND name like '"+startsWith+"%' "; }
				if( (endsWith != null) 		&& (!endsWith.equals("")) 	){ query += " AND name like '%"+endsWith+"' ";	 }
				if( (contains != null) 		&& (!contains.equals("")) 	){ query += " AND name like '%"+contains+"%' ";	 }
				if( (theme != null)         && (!theme.equals(""))      ){ query += " AND theme='"+theme+"' ";           }
				if( (language != null) 		&& (!language.equals("")) 	){ query += " AND language='"+language+"' ";	 }	*/
				
				if(nkFindTwinNameBean.getGender1().equals("B") && nkFindTwinNameBean.getGender2().equals("B")){
					query += " AND gender1='B' AND gender2='B'";
				}else if(nkFindTwinNameBean.getGender1().equals("G") && nkFindTwinNameBean.getGender2().equals("G")){
					query += " AND gender1='G' AND gender2='G'";
				}else if(nkFindTwinNameBean.getGender1().equals("B") && nkFindTwinNameBean.getGender2().equals("G")){
					query += " AND gender1='B' AND gender2='G'";
				}else if(nkFindTwinNameBean.getGender1().equals("G") && nkFindTwinNameBean.getGender2().equals("B")){
					query += " AND gender1='G' AND gender2='B'";
				}
				
				if(!nkFindTwinNameBean.getNameContains().equals(null)){
					query += " AND ( name1 like '%"+nkFindTwinNameBean.getNameContains()+"%' OR name2 like '%"+nkFindTwinNameBean.getNameContains()+"%') " ;
				}
				
				
				System.out.println("NKDAO.findTwinNameDAO(): Query submitted is: "+query);
				ResultSet rs=stmt.executeQuery(query);  
				while(rs.next()){  
					// System.out.println(rs.getString(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
					nkTwinNameBean = new NKTwinNameBean();
					nkTwinNameBean.setName1(rs.getString(1));
					nkTwinNameBean.setName2(rs.getString(2));
					nkTwinNameBean.setMeaning1(rs.getString(3));
					nkTwinNameBean.setMeaning2(rs.getString(4));
					nkTwinNameBean.setGender1(rs.getString(5));
					nkTwinNameBean.setGender2(rs.getString(6));
					alNKTwinNameBean.add(nkTwinNameBean);
				}
				con.close();  
			
			return alNKTwinNameBean;
		}
		catch(Exception e){
			System.out.println("Exception raised in NKDAO.getNameDAO: "+e);
		}	
		return null;	
	}
}
