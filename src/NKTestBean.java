
public class NKTestBean {
	
	// Defining Attributes for the Bean
	String submitter;
	String message;
	String email;
	
	// Defining GETTER & SETTER methods
	public String getSubmitter() {
		return submitter;
	}
	void setSubmitter(String submitter) {
		this.submitter = submitter;
	}
	String getMessage() {
		return message;
	}
	void setMessage(String message) {
		this.message = message;
	}
	String getEmail() {
		return email;
	}
	void setEmail(String email) {
		this.email = email;
	}
	// Overriding toString() method
	public String toString(){
		return "Submitter: "+this.submitter+" Message: "+this.message+" Email: "+this.email;
	}
}
