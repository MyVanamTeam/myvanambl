import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/name")
public class NKNameService {
	
	/* Creating a RESTFUL Web Service using Jersey API */
	/* @GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getNameService")
	public NKNameBean getNameService(){
		// String response = null;
		System.out.println("getNameService() called");
		NKNameBean nameBean = new NKDAO().getNameDAO("Siva");
		if(nameBean != null){
			response = 	"<?xml version='1.0'?>"+"<names>"+"<name>"+nameBean.getName()+"</name>"+
						"<meaning>"+nameBean.getMeaning()+"</meaning>"+"<gender>"+nameBean.getGender()+"</gender>"+"</names>"; 
			// response = "{{name:'Siva'},{meaning:'Name of God'},{gender:'M'}";
			// return nameBean;
		}else{
			// response = 	"<?xml version='1.0'?>"+
			// "<name>Name NOT FOUND</name>";
		}
		// return response;
		return nameBean;
	}*/
	
	/*@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getNameService/{nameArg}")
	public NKNameBean getNameService(@PathParam("nameArg") String nameArg){
		// String response = null;
		System.out.println("getNameService() called");
		NKNameBean nameBean = new NKDAO().getNameDAO(nameArg);
		return nameBean;
	}*/
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getNameService")
	public List<NKNameBean> getNameService(
			@QueryParam("gender") 		String gender,
			@QueryParam("startsWith") 	String startsWith,
			@QueryParam("contains") 	String contains,
			@QueryParam("endsWith") 	String endsWith,
			@QueryParam("theme")		String theme,
			@QueryParam("language")		String language
			){
		System.out.println("NKService.getNameService() called: startsWith: "+startsWith);
		ArrayList<NKNameBean> alNKNameBean = new NKDAO().getNameDAO(gender,startsWith,contains,endsWith,theme,language);
		return alNKNameBean;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/postNameService")
	public List<NKNameBean> postNameService(NKSearchBean nkSearchBean){
		System.out.println("NKService.postNameService() called: startsWith: "+nkSearchBean.getStartsWith()+nkSearchBean.getContains()+nkSearchBean.getGender()+"- THEME: "+nkSearchBean.getTheme()+"- LANGUAGE: "+nkSearchBean.getLanguage());
		ArrayList<NKNameBean> alNKNameBean = new NKDAO().getNameDAO(nkSearchBean.getGender(),
																	nkSearchBean.getStartsWith(),
																	nkSearchBean.getContains(),
																	nkSearchBean.getEndsWith(),
																	nkSearchBean.getTheme(),
																	nkSearchBean.getLanguage()
																	);
		return alNKNameBean;
	}
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/postNameService1")
	public String postNameService1(){
		// System.out.println("NKService.postNameService() called: startsWith: "+nkSearchBean.getStartsWith()+nkSearchBean.getContains()+nkSearchBean.getGender()+"- THEME: "+nkSearchBean.getTheme()+"- LANGUAGE: "+nkSearchBean.getLanguage());
		/* ArrayList<NKNameBean> alNKNameBean = new NKDAO().getNameDAO(nkSearchBean.getGender(),
																	nkSearchBean.getStartsWith(),
																	nkSearchBean.getContains(),
																	nkSearchBean.getEndsWith(),
																	nkSearchBean.getTheme(),
																	nkSearchBean.getLanguage()
																	);*/
		return "TestString";
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/putNameService")
	public Boolean putNameService(NKNameBean nkNameBean){
		System.out.println("NKNameService.putNameService() called: ..... "+nkNameBean);
		Boolean result = new NKDAO().putNameDAO(nkNameBean);
		return result;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findNameService")
	public List<NKNameBean> findNameService(NKFindNameBean nkFindNameBean){
		System.out.println("NKService.findNameService() called:");
		ArrayList<NKNameBean> alNKNameBean = new NKDAO().findNameDAO(nkFindNameBean);
		return alNKNameBean;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/findTwinNameService")
	public List<NKTwinNameBean> findTwinNameService(NKFindTwinNameBean nkFindTwinNameBean){
		System.out.println("NKService.findTwinNameService() called:");
		ArrayList<NKTwinNameBean> alNKTwinNameBean = new NKDAO().findTwinNameDAO(nkFindTwinNameBean);
		return alNKTwinNameBean;
	}
}