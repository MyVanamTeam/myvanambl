
public class NKTwinNameBean {

	String name1;
	String name2;
	String meaning1;
	String meaning2;
	String gender1;
	String gender2;
	
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	public String getMeaning1() {
		return meaning1;
	}
	public void setMeaning1(String meaning1) {
		this.meaning1 = meaning1;
	}
	public String getMeaning2() {
		return meaning2;
	}
	public void setMeaning2(String meaning2) {
		this.meaning2 = meaning2;
	}
	public String getGender1() {
		return gender1;
	}
	public void setGender1(String gender1) {
		this.gender1 = gender1;
	}
	public String getGender2() {
		return gender2;
	}
	public void setGender2(String gender2) {
		this.gender2 = gender2;
	}
	
	
}
