
public class NKFindTwinNameBean {
	
	String gender1;
	String gender2;
	String nameContains;
	String meaningContains;
	String similarityBy;
	public String getGender1() {
		return gender1;
	}
	public void setGender1(String gender1) {
		this.gender1 = gender1;
	}
	public String getGender2() {
		return gender2;
	}
	public void setGender2(String gender2) {
		this.gender2 = gender2;
	}
	public String getNameContains() {
		return nameContains;
	}
	public void setNameContains(String nameContains) {
		this.nameContains = nameContains;
	}
	public String getMeaningContains() {
		return meaningContains;
	}
	public void setMeaningContains(String meaningContains) {
		this.meaningContains = meaningContains;
	}
	public String getSimilarityBy() {
		return similarityBy;
	}
	public void setSimilarityBy(String similarityBy) {
		this.similarityBy = similarityBy;
	}
	public String toString(){
		return "GENDER1: "+this.gender1+" GENDER2: "+this.gender2+" nameContains: "+this.nameContains;
	}
}
