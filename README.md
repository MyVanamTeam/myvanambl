**Steps Involved:**

1. Install eclipse.
2. Create a dynamic web project "Namakaranam WP". 
3. Download Jersey 2.0 JAR files. from https://jax-rs-spec.java.net
4. Download Jenson 0.98 JAR from http://www.java2s.com/Code/Jar/g/Downloadgenson098jar.htm
5. Download and Install MySQL community server and also its workbench from https://dev.mysql.com/downloads/
6. Download and Add MySQL JAR mysql.java connector .5.1.41

**Steps in MySQL to create database(below steps are mac related, I think you know Windows steps).** 

* 1. For interacting with the database via terminal:
* 1. Open the terminal.
* 1. cd /usr/local/mysql/bin
* 1. ./mysql -u root -p   …………. Then press enter
* 1. Give password……..
* 1. Then it opens mysql> command prompt.
* 1. Show databases;
* 1. Create database namakaranam;
* 1. Show database; 		=> Now it should show namakaram in the list.
* 1. Use namakaranam;
* 1. Show tables;			=> It will show empty set.
* 1. create table name(recid int, name varchar(25), meaning varchar(50), gender varchar(1), theme varchar(20), language varchar(20));                      	=> Creates new table in the database.
* 1. insert into name values(1,'Aditya','Name of Sun','M','Gods','ANY'); 	=> inserts new record.
* 1. Select * from name;		=> displays the record.
* 1. insert into name values(2,'Vishnu','Name of Lord','M','Gods','ANY');
* 1. insert into name values(3,'Lakshmi','Name of Goddess','F','Gods','ANY');
* 1. insert into name values(4,'Siva','Name of Goddess','F','Gods','ANY');
* 1. insert into name values(5,'Durga','Name of Goddess','F','Gods','ANY');
* 1. update name set meaning = 'Name of God', gender='M' WHERE name = 'Siva';
* 1. select * from name;













# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact